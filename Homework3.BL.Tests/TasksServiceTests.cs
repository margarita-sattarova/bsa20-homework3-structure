﻿using Homework3.BL.Tests.Fake.UnitOfWork;
using System;
using System.Linq;
using Xunit;
using FakeItEasy;
using Homework3.DAL.Entities;
using Homework3.BLL.Services;
using Homework3.DAL.Interfaces;
using System.Threading.Tasks;

namespace Homework3.BL.Tests
{
	public class TasksServiceTests
	{
		private IUnitOfWork _fakeUnitOfWork;
		private TasksService _tasksService;

		public TasksServiceTests()
		{
			_fakeUnitOfWork = new FakeUnitOfWork();
			_tasksService = new TasksService(_fakeUnitOfWork);
		}

		[Theory(DisplayName = "Setting task state to finished (Checking if Repository Update method is called)")]
		[InlineData(2, 2)]
		public async System.Threading.Tasks.Task Update_WhenCalled_ThenCallRepositoryUpdate(int searchedTaskId, int taskStateToSet)
		{
			_fakeUnitOfWork = A.Fake<IUnitOfWork>();
			_tasksService = new TasksService(_fakeUnitOfWork);

			var searchedTask = await _tasksService.GetTaskById(searchedTaskId);
			var newTaskInfo = new DAL.Entities.Task()
			{
				Name = searchedTask.Name,
				Description = searchedTask.Description,
				PerformerId = searchedTask.PerformerId,
				State = taskStateToSet,
				CreatedAt = searchedTask.CreatedAt,
				ProjectId = searchedTask.ProjectId
			};

			await _tasksService.Update(newTaskInfo, searchedTaskId);

			A.CallTo(() => _fakeUnitOfWork.Tasks.Update(newTaskInfo, searchedTaskId)).MustHaveHappenedOnceExactly();
		}

		[Theory(DisplayName = "Setting task state to finished (Checking if property FinishedAt has changed)")]
		[InlineData(5, 2)]
		public async System.Threading.Tasks.Task Update_WhenCalledAndTaskWasNotFinished_ThenFinishedAtValueChanged(int searchedTaskId, int taskStateToSet)
		{
			var searchedTask = await _tasksService.GetTaskById(searchedTaskId);
			var initialFinishedAt = searchedTask.FinishedAt;
			var newTaskInfo = new DAL.Entities.Task()
			{
				Name = searchedTask.Name,
				Description = searchedTask.Description,
				PerformerId = searchedTask.PerformerId,
				State = taskStateToSet,
				CreatedAt = searchedTask.CreatedAt,
				ProjectId = searchedTask.ProjectId
			};

			await _tasksService.Update(newTaskInfo, searchedTaskId);
			var searchedTaskAfterModification = await _tasksService.GetTaskById(searchedTaskId);

			Assert.NotEqual(initialFinishedAt, searchedTaskAfterModification.FinishedAt);
		}

		[Theory]
		[InlineData(250)]
		public async System.Threading.Tasks.Task GetUserTasks_WhenInvalidId_ThenThrowArgumentException(int searchedUserId)
		{
			await Assert.ThrowsAsync<ArgumentException>(() => _tasksService.GetUserTasks(searchedUserId));
		}

		[Theory]
		[InlineData(3, new int[] { 167, 194 })]
		[InlineData(43, new int[0])]
		public async System.Threading.Tasks.Task GetUserTasks_WhenIdIsValid_ThenReturnExpectedResult(int userId, int[] tasksId)
		{
			var result = await _tasksService.GetUserTasks(userId);
			var actualTasksId = result.Select(x => x.Id).ToArray();

			Assert.Equal(tasksId, actualTasksId);
		}

		[Theory]
		[InlineData(13, new int[] { 92, 114, 139, 145, 150, 192 })]
		[InlineData(18, new int[] { 15, 125 })]
		public async System.Threading.Tasks.Task GetUserNotFinishedTasks_WhenIdIsValid_ThenReturnExpectedResult(int userId, int[] tasksId)
		{
			var result = await _tasksService.GetUserNotFinishedTasks(userId);
			var actualTasksId = result.Select(x => x.Id).ToArray();

			Assert.Equal(tasksId, actualTasksId);
		}

		[Theory]
		[InlineData(163)]
		public async System.Threading.Tasks.Task GetUserNotFinishedTasks_WhenInvalidId_ThenThrowArgumentException(int searchedUserId)
		{
			await Assert.ThrowsAsync<ArgumentException>(() => _tasksService.GetUserNotFinishedTasks(searchedUserId));
		}

		[Theory]
		[InlineData(75)]
		public async System.Threading.Tasks.Task GetUserTasksFinishedIn2020_WhenInvalidId_ThenThrowArgumentException(int searchedUserId)
		{
			await Assert.ThrowsAsync<ArgumentException>(() => _tasksService.GetUserTasksFinishedIn2020(searchedUserId));
		}

		[Theory]
		[InlineData(15, 2020)]
		[InlineData(21, 2020)]
		public async System.Threading.Tasks.Task GetUserTasksFinishedIn2020_WhenIdIsValidAndUserHasFinishedTasks_ThenFinishedAtYearIs2020(int userId, int expectedFinishedAtYear)
		{
			var result = await _tasksService.GetUserTasksFinishedIn2020(userId);

			Assert.True(result.TrueForAll(x => x.FinishedAt.Year == expectedFinishedAtYear));
		}
	}
}
