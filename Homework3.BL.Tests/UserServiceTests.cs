using Homework3.BLL.Services;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using Xunit;
using FakeItEasy;
using Homework3.BL.Tests.Fake.UnitOfWork;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Homework3.BL.Tests
{
    public class UserServiceTests
    {
        private IUnitOfWork _fakeUnitOfWork;
        private UserService _userService;

        public UserServiceTests()
        {
            _fakeUnitOfWork = new FakeUnitOfWork();
            _userService = new UserService(_fakeUnitOfWork);
        }

        [Fact]
        public async System.Threading.Tasks.Task Create_WhenCalled_ThenCallRepositoryCreate()
        {
            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _userService = new UserService(_fakeUnitOfWork);
            var user = new User()
            {
                FirstName = "Elen",
                LastName = "Smith",
                Email = "some_name@gmail.com",
                Birthday = new DateTime(2018, 7, 24, 19, 10, 18, 434, DateTimeKind.Local).AddTicks(5195),
                RegisteredAt = new DateTime(2020, 6, 13, 9, 50, 19, 517, DateTimeKind.Local).AddTicks(9342)
            };

            await _userService.Create(user);

            A.CallTo(() => _fakeUnitOfWork.Users.Create(A<User>.That.IsInstanceOf(typeof(User)))).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async System.Threading.Tasks.Task Create_WhenCalledWithNullArgument_ThenThrowArgumentNullException()
        {
            User user = null;

            await Assert.ThrowsAsync<ArgumentNullException>(() => _userService.Create(user));
        }

        [Theory(DisplayName = "Adding member to a team (Positive case)")]
        [InlineData(1, 2)]
        public async System.Threading.Tasks.Task Update_WhenCalledWithExistingTeamId_ThenCallRepositoryUpdate(int searchedUserId, int teamIdToAddMember)
        {
            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _userService = new UserService(_fakeUnitOfWork);
            var searchedUser = await _userService.GetUserById(searchedUserId);
            var newUserInfo = new User()
            {
                FirstName = searchedUser.FirstName,
                LastName = searchedUser.LastName,
                Email = searchedUser.Email,
                Birthday = searchedUser.Birthday,
                RegisteredAt = searchedUser.RegisteredAt,
                TeamId = teamIdToAddMember
            };

            await _userService.Update(newUserInfo, searchedUserId);

            A.CallTo(() => _fakeUnitOfWork.Users.Update(newUserInfo, searchedUserId)).MustHaveHappenedOnceExactly();
        }

        [Theory(DisplayName = "Adding member to a team (Negative case)")]
        [InlineData(3, 100)]
        public async System.Threading.Tasks.Task Update_WhenCalledWithNotExistingTeamId_ThenThrowArgumentException(int searchedUserId, int teamIdToAddMember)
        {
            var searchedUser = await _userService.GetUserById(searchedUserId);
            var newUserInfo = new User()
            {
                FirstName = searchedUser.FirstName,
                LastName = searchedUser.LastName,
                Email = searchedUser.Email,
                Birthday = searchedUser.Birthday,
                RegisteredAt = searchedUser.RegisteredAt,
                TeamId = teamIdToAddMember
            };

            await Assert.ThrowsAsync<ArgumentException>(() => _fakeUnitOfWork.Users.Update(newUserInfo, searchedUserId));
        }

        [Theory]
        [InlineData(2)]
        public async System.Threading.Tasks.Task GetUserDataStrusture_WhenUserDoesNotHaveProjects_ThenThrowInvalidOperationException(int userWithNoProjectsId)
        {           
            await Assert.ThrowsAsync<InvalidOperationException>(() => _userService.GetUserDataStrusture(userWithNoProjectsId));
        }

        [Theory]
        [InlineData(41, "Voluptate voluptatibus quidem eligendi magni.", 142)]
        [InlineData(13, "Quam veniam distinctio ut magnam.", 114)]
        public async System.Threading.Tasks.Task GetUserDataStrusture_WhenUserHasProjects_ThenReturnExpectedResult(int userId, string lastProjectName, int longestTaskId)
        {
            var result = await _userService.GetUserDataStrusture(userId);

            Assert.Equal(lastProjectName, result.LastProject.Name);
            Assert.Equal(longestTaskId, result.LongestTask.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUsersWithTasksSorted_WhenCalled_ThenResultIsSortedInTheCorrectOrderByUserName()
        {
            var usersWithTasksSorted = await _userService.GetUsersWithTasksSorted();
            var actual = usersWithTasksSorted.Select(x => x.User.FirstName).ToList();
            var expected = usersWithTasksSorted.Select(x => x.User.FirstName).OrderBy(x => x).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUsersWithTasksSorted_WhenCalled_ThenResultIsSortedInTheCorrectOrderByTasksNameLength()
        {
            var usersWithTasksSorted = await _userService.GetUsersWithTasksSorted();
            var actual = usersWithTasksSorted.Select(x => x.Tasks).ToList();
            var expected = new List<List<DAL.Entities.Task>>();

            foreach(var item in usersWithTasksSorted)
            {
                expected.Add(item.Tasks.OrderByDescending(t => t.Name.Length).ToList());
            }

            Assert.Equal(expected, actual);
        }
    }
}
