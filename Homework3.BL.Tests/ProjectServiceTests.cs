﻿using Homework3.BL.Tests.Fake.UnitOfWork;
using Homework3.BLL;
using Homework3.DAL.Interfaces;
using System.Linq;
using Xunit;
using System.Threading.Tasks;

namespace Homework3.BL.Tests
{
    public class ProjectServiceTests
    {
        private readonly IUnitOfWork _fakeUnitOfWork;
        private readonly ProjectService _projectService;

        public ProjectServiceTests()
        {
            _fakeUnitOfWork = new FakeUnitOfWork();
            _projectService = new ProjectService(_fakeUnitOfWork);
        }

        [Theory]
        [InlineData(2)]
        public async Task GetNumberOfTasksOnUsersProjects_WhenUserDoesNotHaveProjects_ThenEmptyListReturned(int searchedUserId)
        {
            var actual = await _projectService.GetNumberOfTasksOnUsersProjects(searchedUserId);

            Assert.Empty(actual);
        }

        [Theory]
        [InlineData(1, new int[] { 24 }, new int[] { 1 })]
        [InlineData(4, new int[] { 16, 33 }, new int[] { 3, 1 })]
        public async Task GetNumberOfTasksOnUsersProjects_WhenUserHaveProjectsWithTasks_ThenReturnExpectedResult(int userId, int[] projectsId, int[] amountsOfTasks)
        {
            var result = await _projectService.GetNumberOfTasksOnUsersProjects(userId);
            var actualProjectsId = result.Select(x => x.ProjectId).ToArray();
            var actualAmountsOfTasks = result.Select(x => x.TasksCount).ToArray();

            Assert.Equal(projectsId, actualProjectsId);
            Assert.Equal(amountsOfTasks, actualAmountsOfTasks);
        }

        [Fact]
        public async Task GetProjectsDataStructure_WhenCalled_ThenReturnExpectedCount()
        {
            var projects = await _projectService.GetProjects();
            var expectedCount = projects.Count;

            var projectsDataStructure = await _projectService.GetProjectsDataStructure();
            var actualCount = projectsDataStructure.Count;

            Assert.Equal(expectedCount, actualCount);
        }

        [Theory]
        [InlineData(40, 114, 183, 3)]
        [InlineData(99, 38, 188, 0)]
        public async Task GetProjectsDataStructure_WhenCalled_ThenReturnExpectedResult(int projectId, int tasksWithLongestDescriptionId, int tasksWithShortestNameId, int teamMembersCount)
        {
            var result = await _projectService.GetProjectsDataStructure();
            var actualTasksWithLongestDescriptionId = result.FirstOrDefault(x => x.Project.Id == projectId).TaskWithLongestDescription.Id;
            var actualTasksWithShortestNameId = result.FirstOrDefault(x => x.Project.Id == projectId).taskWithShortestName.Id;
            var actualTeamMembersCount = result.FirstOrDefault(x => x.Project.Id == projectId).TeamMembersCount;

            Assert.Equal(tasksWithLongestDescriptionId, actualTasksWithLongestDescriptionId);
            Assert.Equal(tasksWithShortestNameId, actualTasksWithShortestNameId);
            Assert.Equal(teamMembersCount, actualTeamMembersCount);
        }
    }
}
