﻿using Homework3.BL.Tests.Fake.UnitOfWork;
using Xunit;
using Homework3.BLL.Services;
using Homework3.DAL.Interfaces;
using System.Threading.Tasks;

namespace Homework3.BL.Tests
{
    public class TeamServiceTests
    {
        private readonly IUnitOfWork _fakeUnitOfWork;
        private readonly TeamService _teamService;

        public TeamServiceTests()
        {
            _fakeUnitOfWork = new FakeUnitOfWork();
            _teamService = new TeamService(_fakeUnitOfWork);
        }

        [Fact]
        public async Task GetTeamMembersOlder10_WhenCalled_ThenEachUserBirthdateYearIsLessThan2010()
        {
            var result = await _teamService.GetTeamMembersOlder10();

            Assert.True(result.TrueForAll(x => x.Users.TrueForAll(u => u.Birthday.Year < 2010)));
        }
    }
}
