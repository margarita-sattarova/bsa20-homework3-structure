﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;

namespace Homework3.BL.Tests.Fake.Repository
{
    public class FakeProjectRepository : FakeRepository<Project>
    {
        public FakeProjectRepository()
        {
            entities.AddRange(DataSeed.Projects);
        }

        public override async System.Threading.Tasks.Task Update(Project entity, int id)
        {
            var temp = entities.FindIndex(x => x.Id == id);

            if (entities[temp] != null)
            {
                entities[temp].Name = entity.Name;
                entities[temp].Description = entity.Description;
                entities[temp].TeamId = entity.TeamId;
                entities[temp].Deadline = entity.Deadline;
            }
        }
    }
}
