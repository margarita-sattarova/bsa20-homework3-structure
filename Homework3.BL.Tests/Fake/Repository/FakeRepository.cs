﻿using Homework3.DAL.Interfaces;
using System.Collections.Generic;
using Homework3.DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.BL.Tests.Fake.Repository
{
    public class FakeRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        public List<TEntity> entities = new List<TEntity>();

        public FakeRepository()
        {

        }

        public virtual async System.Threading.Tasks.Task Create(TEntity entity)
        {
            entity.Id = entities.Count + 1;
            entities.Add(entity);
        }

        public virtual async System.Threading.Tasks.Task Delete(TEntity entity)
        {
            entities.Remove(entity);
        }

        public virtual async System.Threading.Tasks.Task Delete(int id)
        {
            entities.RemoveAt(id);
        }

        public virtual async Task<List<TEntity>> Get()
        {
            return entities;
        }

        public virtual async Task<TEntity> Get(int id) => entities.FirstOrDefault(x => x.Id == id);

        public virtual async System.Threading.Tasks.Task Update(TEntity entity, int id)
        {
            var temp = entities.FindIndex(x => x.Id == id);
            entities[temp] = entity;
        }
    }
}
