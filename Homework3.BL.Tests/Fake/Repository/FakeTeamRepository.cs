﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;

namespace Homework3.BL.Tests.Fake.Repository
{
    public class FakeTeamRepository : FakeRepository<Team>
    {
        public FakeTeamRepository()
        {
            entities.AddRange(DataSeed.Teams);
        }
        public override async System.Threading.Tasks.Task Update(Team entity, int id)
        {
            var temp = entities.FindIndex(x => x.Id == id);

            if (entities[temp] != null)
            {
                entities[temp].Name = entity.Name;
            }
        }
    }
}
