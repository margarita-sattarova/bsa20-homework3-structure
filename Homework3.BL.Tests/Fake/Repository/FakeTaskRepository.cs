﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using System;

namespace Homework3.BL.Tests.Fake.Repository
{
    public class FakeTaskRepository : FakeRepository<Task>
    {
        public FakeTaskRepository()
        {
            entities.AddRange(DataSeed.Tasks);
        }
        public override async System.Threading.Tasks.Task Update(DAL.Entities.Task entity, int id)
        {
            var temp = entities.FindIndex(x => x.Id == id);

            if (entities[temp] != null)
            {
                if (entity.State == 2 && entities[temp].State != 2)
                {
                    entities[temp].FinishedAt = DateTime.Now;
                }

                entities[temp].PerformerId = entity.PerformerId;
                entities[temp].Name = entity.Name;
                entities[temp].Description = entity.Description;
                entities[temp].State = entity.State;
                entities[temp].ProjectId = entity.ProjectId;
            }
        }
    }
}
