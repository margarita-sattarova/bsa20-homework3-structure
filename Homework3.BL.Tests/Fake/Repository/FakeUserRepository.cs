﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using System;
using System.Linq;

namespace Homework3.BL.Tests.Fake.Repository
{
	public class FakeUserRepository : FakeRepository<User>
	{
		public FakeTeamRepository fakeTeamRepos = new FakeTeamRepository();

		public FakeUserRepository()
		{
			entities.AddRange(DataSeed.Users);
		}

		public override async System.Threading.Tasks.Task Update(User entity, int id)
		{
			if (fakeTeamRepos.entities.FirstOrDefault(x => x.Id == entity.TeamId) == null) throw new ArgumentException("Team with Id provided in property 'TeamId' does not exist.", "entity");

			var temp = entities.FindIndex(x => x.Id == id);

			if (entities[temp] != null)
			{
				entities[temp].FirstName = entity.FirstName;
				entities[temp].LastName = entity.LastName;
				entities[temp].Birthday = entity.Birthday;
				entities[temp].Email = entity.Email;
				entities[temp].TeamId = entity.TeamId;
			}
		}
	}
}
