﻿using Homework3.BL.Tests.Fake.Repository;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace Homework3.BL.Tests.Fake.UnitOfWork
{
    public class FakeUnitOfWork : IUnitOfWork
    {
        private readonly FakeProjectRepository projectRepository;
        private readonly FakeTaskRepository taskRepository;
        private readonly FakeTeamRepository teamRepository;
        private readonly FakeUserRepository userRepository;

        public FakeUnitOfWork()
        {
            projectRepository = new FakeProjectRepository();
            taskRepository = new FakeTaskRepository();
            teamRepository = new FakeTeamRepository();
            userRepository = new FakeUserRepository();
        }

        public IRepository<Project> Projects => projectRepository;

        public IRepository<DAL.Entities.Task> Tasks => taskRepository;

        public IRepository<Team> Teams => teamRepository;

        public IRepository<User> Users => userRepository;

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }
    }
}
