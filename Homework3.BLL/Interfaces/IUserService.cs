﻿using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.BLL.Interfaces
{
    public interface IUserService
    {
        System.Threading.Tasks.Task Create(User user);
        Task<List<User>> GetUsers();
        Task<User> GetUserById(int id);
        Task<List<UserWithTasksSortedDto>> GetUsersWithTasksSorted();
        System.Threading.Tasks.Task Delete(int id);
        Task<UserDataStructureDto> GetUserDataStrusture(int id);
        System.Threading.Tasks.Task Update(User newUserInfo, int id);
    }
}
