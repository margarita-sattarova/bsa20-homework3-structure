﻿using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.BLL.Interfaces
{
    public interface ITeamService
    {
        System.Threading.Tasks.Task Create(Team team);
        Task<List<Team>> GetTeams();
        Task<Team> GetTeamById(int id);
        Task<List<TeamsMembersDto>> GetTeamMembersOlder10();
        System.Threading.Tasks.Task Delete(int id);
        System.Threading.Tasks.Task Update(Team newTeamInfo, int id);

    }
}
