﻿using Homework3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.BLL.Interfaces
{
    public interface ITasksService
    {
        System.Threading.Tasks.Task Create(DAL.Entities.Task task);
        Task<List<DAL.Entities.Task>> GetTasks();
        Task<DAL.Entities.Task> GetTaskById(int id);
        Task<List<DAL.Entities.Task>> GetUserTasks(int id);
        Task<List<DAL.Entities.Task>> GetUserTasksFinishedIn2020(int id);
        System.Threading.Tasks.Task Delete(int id);
        System.Threading.Tasks.Task Update(DAL.Entities.Task newTaskInfo, int id);
        Task<List<DAL.Entities.Task>> GetUserNotFinishedTasks(int id);

    }
}
