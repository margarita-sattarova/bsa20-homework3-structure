﻿using Homework3.Common;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.BLL.Interfaces
{
    public interface IProjectService
    {
        System.Threading.Tasks.Task Create(Project project);
        Task<List<Project>> GetProjects();
        Task<Project> GetProjectById(int id);
        Task<List<UserProjectsTasksDto>> GetNumberOfTasksOnUsersProjects(int id);
        System.Threading.Tasks.Task Delete(int id);
        Task<List<ProjectDataStructureDto>> GetProjectsDataStructure();
        System.Threading.Tasks.Task Update(Project newProjectInfo, int id);

    }
}
