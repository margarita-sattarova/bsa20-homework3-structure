﻿using Homework3.BLL.Interfaces;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.BLL.Services
{
    public class TasksService : ITasksService
    {
        readonly IUnitOfWork _unit;

        public TasksService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public async System.Threading.Tasks.Task Create(DAL.Entities.Task task)
        {
            await _unit.Tasks.Create(task);
        }

        public async Task<List<DAL.Entities.Task>> GetTasks()
        {
            return await _unit.Tasks.Get();
        }

        public async Task<DAL.Entities.Task> GetTaskById(int id)
        {
            return await _unit.Tasks.Get(id);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var tasks = await _unit.Tasks.Get();
            var searchedItem = tasks.FirstOrDefault(x => x.Id == id);

            if (searchedItem == null) throw new ArgumentException("Task with such Id does not exist.", "id");

            await _unit.Tasks.Delete(id);
        }

        public async System.Threading.Tasks.Task Update(DAL.Entities.Task newTaskInfo, int id)
        {
            await _unit.Tasks.Update(newTaskInfo, id);
        }

        public async Task<List<DAL.Entities.Task>> GetUserTasks(int id)
        {
            var users = await _unit.Users.Get();
            var searchedItem = users.FirstOrDefault(x => x.Id == id);
            if (searchedItem == null) throw new ArgumentException("User with such Id does not exist.", "id");

            var tasks = await _unit.Tasks.Get();
            var tasksQuery = tasks
                        .Where(x => (x.PerformerId.Equals(id) && x.Name.Length < 45))
                        .ToList();

            return tasksQuery;
        }

        public async Task<List<DAL.Entities.Task>> GetUserNotFinishedTasks(int id)
        {
            var users = await _unit.Users.Get();
            var searchedItem = users.FirstOrDefault(x => x.Id == id);
            if (searchedItem == null) throw new ArgumentException("User with such Id does not exist.", "id");

            var tasks = await _unit.Tasks.Get();
            var tasksQuery = tasks
                        .Where(x => (x.PerformerId.Equals(id) && x.State != 2))
                        .ToList();

            return tasksQuery;
        }

        public async Task<List<DAL.Entities.Task>> GetUserTasksFinishedIn2020(int id)
        {
            var users = await _unit.Users.Get();
            var searchedItem = users.FirstOrDefault(x => x.Id == id);
            if (searchedItem == null) throw new ArgumentException("User with such Id does not exist.", "id");

            var tasks = await _unit.Tasks.Get();
            var tasksQuery = tasks
                        .Where(x => (x.PerformerId.Equals(id) && x.State == 2 && x.FinishedAt.Year == 2020))
                        .ToList();

            return tasksQuery;
        }

    }
}
