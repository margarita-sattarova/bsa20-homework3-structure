﻿using Homework3.BLL.Interfaces;
using Homework3.Common;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.BLL
{
    public class ProjectService : IProjectService
    {
        readonly IUnitOfWork _unit;
        //private List<User> _users;
        //private List<Project> _projects;
        //private List<DAL.Entities.Task> _tasks;
        //private List<Team> _teams;

        public ProjectService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public async System.Threading.Tasks.Task Create(Project project)
        {
            if (string.IsNullOrWhiteSpace(project.Name) || string.IsNullOrWhiteSpace(project.Description)) throw new ArgumentException("Project name and description cannot be empty strings.", "project");

            var searchedItem = await _unit.Users.Get(project.AuthorId);
            if (searchedItem == null) throw new ArgumentException("User with Id provided in property 'AuthorId' does not exist.", "project");

            await _unit.Projects.Create(project);
        }

        public async Task<List<Project>> GetProjects()
        {
            return await _unit.Projects.Get();
        }

        public async Task<Project> GetProjectById(int id)
        {
            return await _unit.Projects.Get(id);
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _unit.Projects.Delete(id);
        }

        public async System.Threading.Tasks.Task Update(Project newProjectInfo, int id)
        {
            await _unit.Projects.Update(newProjectInfo, id);
        }

        public async Task<List<UserProjectsTasksDto>> GetNumberOfTasksOnUsersProjects(int id)
        {
            var _users = await _unit.Users.Get();
            var searchedItem = _users.FirstOrDefault(x => x.Id == id);

            if (searchedItem == null) throw new ArgumentException("User with such Id does not exist.", "id");

            var _projects = await _unit.Projects.Get();
            var _tasks = await _unit.Tasks.Get();
            var tasksQuery = _projects
                    .Where(item => item.AuthorId == id)
                    .Join(_tasks, p => p.Id, t => t.ProjectId, (p, t) => (p, t))
                    .GroupBy(tuple => tuple.p)
                    .ToDictionary(x => x.Key, x => x.Count(tup => tup.p.Id == tup.t.ProjectId));

            var result = new List<UserProjectsTasksDto>();

            foreach (var item in tasksQuery)
            {
                result.Add(new UserProjectsTasksDto(item.Key.Id, item.Key.Name, item.Key.Description, item.Value));
            }

            return result;
        }

        public async Task<List<ProjectDataStructureDto>> GetProjectsDataStructure()
        {
            var projects = await _unit.Projects.Get();
            var tasks = await _unit.Tasks.Get();
            var users = await _unit.Users.Get();

            var teamMembers = projects
                    .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { Project = p, Tasks = t })
                    .Where(x => x.Project.Description.Length > 20 || x.Tasks.Count() < 3)
                    .GroupJoin(users, gj => gj.Project.TeamId, user => user.TeamId, (gj, user) => new { ProjectId = gj.Project.Id, Users = user })
                    .ToDictionary(x => x.ProjectId, y => y.Users.Count());

            var tasksWithLongestDescription = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { ProjectId = p.Id, Tasks = t })
                .Select(p => p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2))
                .ToArray();

            var tasksWithShortestName = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { ProjectId = p.Id, Tasks = t })
                .Select(p => p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((t1, t2) => t1.Name.Length < t2.Name.Length ? t1 : t2))
                .ToArray();

            var result = new List<ProjectDataStructureDto>();

            for (int i = 0; i < projects.Count; i++)
            {
                var newItem = new ProjectDataStructureDto(projects[i], tasksWithLongestDescription[i], tasksWithShortestName[i]);

                if (teamMembers.ContainsKey(projects[i].Id))
                {
                    newItem.TeamMembersCount = teamMembers[projects[i].Id];

                }

                result.Add(newItem);
            }

            return result;
        }
    }
}
