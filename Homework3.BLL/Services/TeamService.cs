﻿using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Homework3.BLL.Interfaces;
using Homework3.Common.DTOs;
using System.Threading.Tasks;

namespace Homework3.BLL.Services
{
    public class TeamService : ITeamService
    {
        readonly IUnitOfWork _unit;

        public TeamService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public async System.Threading.Tasks.Task Create(Team team)
        {
            if (string.IsNullOrWhiteSpace(team.Name)) throw new ArgumentException("Team name cannot be an empty string.", "team");

            await _unit.Teams.Create(team);
        }

        public async Task<List<Team>> GetTeams()
        {
            return await _unit.Teams.Get();
        }

        public async Task<Team> GetTeamById(int id)
        {
            return await _unit.Teams.Get(id);
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _unit.Teams.Delete(id);
        }

        public async System.Threading.Tasks.Task Update(Team newTeamInfo, int id)
        {
            await _unit.Teams.Update(newTeamInfo, id);
        }

        public async Task<List<TeamsMembersDto>> GetTeamMembersOlder10()
        {
            var users = await _unit.Users.Get();
            var teams = await _unit.Teams.Get();
            var teamMembersQuery = users
                                   .Where(u => u.Birthday.Year < 2010)
                                   .Join(teams, u => u.TeamId, t => t.Id, (u, t) => (team: t, u))
                                   .OrderByDescending(x => x.u.RegisteredAt)
                                   .GroupBy(x => x.u.TeamId);

            var result = new List<TeamsMembersDto>();

            foreach(var item in teamMembersQuery)
            {
                var newTeam = new TeamsMembersDto(item.Key, item.FirstOrDefault().team.Name);

                foreach(var (team, u) in item)
                {
                    if (u != null)
                        newTeam.Users.Add(u);
                }

                result.Add(newTeam);
            }

            return result;
        }
    }
}
