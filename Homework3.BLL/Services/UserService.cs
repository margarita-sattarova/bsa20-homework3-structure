﻿using Homework3.BLL.Interfaces;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.BLL.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork _unit;

        public UserService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public async System.Threading.Tasks.Task Create(User user)
        {
            if (user == null) throw new ArgumentNullException("user", "User to create cannot be null.");

            var teams = await _unit.Teams.Get();
            if (user.TeamId > teams.Count) throw new ArgumentException("Team with Id provided for property 'TeamId' does not exist.", "user");

            await _unit.Users.Create(user);
        }

        public async Task<List<User>> GetUsers()
        {
            return await _unit.Users.Get();
        }

        public async Task<User> GetUserById(int id)
        {
            return await _unit.Users.Get(id);
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var users = await _unit.Users.Get();
            if (users.FirstOrDefault(x => x.Id == id) == null) throw new ArgumentException("User with such Id does not exist.", "id");

            await _unit.Users.Delete(id);
        }

        public async System.Threading.Tasks.Task Update(User newUserInfo, int id)
        {          
            await _unit.Users.Update(newUserInfo, id);
        }

        public async Task<List<UserWithTasksSortedDto>> GetUsersWithTasksSorted()
        {
            var users = await _unit.Users.Get();
            var tasks = await _unit.Tasks.Get();
            var usersQ = users
                         .Join(tasks, u => u.Id, t => t.PerformerId, (u, t) => (user: u, t))
                         .OrderByDescending(x => x.t.Name.Length)
                         .GroupBy(x => x.user)
                         .OrderBy(x => x.Key.FirstName);

            var result = new List<UserWithTasksSortedDto>();

            foreach(var item in usersQ)
            {
                var newUser = new UserWithTasksSortedDto(item.Key);

                foreach(var (user, t) in item)
                {
                    if (t != null)
                        newUser.Tasks.Add(t);
                }

                result.Add(newUser);
            }

            return result;
        }

        public async Task<UserDataStructureDto> GetUserDataStrusture(int id)
        {
            var users = await _unit.Users.Get();
            var user = users
                       .FirstOrDefault(u => u.Id == id);

            var projects = await _unit.Projects.Get();
            var tasks = await _unit.Tasks.Get();
            var searchedProject = projects.FirstOrDefault(x => x.AuthorId == id);
            if (searchedProject == null) throw new InvalidOperationException("User with such Id has not authored any projects.");

            var lastProject = projects
                              .Where(p => p.AuthorId == id)
                              .Aggregate((p1, p2) => p1.CreatedAt > p2.CreatedAt ? p1 : p2);

            var countOfUsersTasks = tasks.Count(t => t.PerformerId == id);

            var countOfUsersNotFinishedTasks = tasks
                                              .Where(t => t.PerformerId == id)
                                              .Count(t => t.State != 2);

            var longestUsersTask = tasks
                                   .Where(x => x.PerformerId == id)
                                   .Aggregate((p1, p2) => (p1.FinishedAt - p1.CreatedAt) > (p2.FinishedAt - p2.CreatedAt) ? p1 : p2);

            var result = new UserDataStructureDto(user, lastProject, countOfUsersTasks, countOfUsersNotFinishedTasks, longestUsersTask);

            return result;            
        }
    }
}
