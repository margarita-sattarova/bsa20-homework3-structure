﻿using Newtonsoft.Json;

namespace Homework3.Common
{
    public class UserProjectsTasksDto
    {
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("projectDescription")]
        public string ProjectDescription { get; set; }

        [JsonProperty("tasksCount")]
        public int TasksCount { get; set; }

        public UserProjectsTasksDto(int id, string name, string desc, int count)
        {
            ProjectId = id;
            ProjectName = name;
            ProjectDescription = desc;
            TasksCount = count;
        }
    }
}
