﻿using Homework3.DAL.Entities;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Homework3.Common.DTOs
{
    public class UserWithTasksSortedDto
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("tasks")]
        public List<Task> Tasks { get; set; } = new List<Task>();

        public UserWithTasksSortedDto(User user)
        {
            User = user;
        }
    }
}
