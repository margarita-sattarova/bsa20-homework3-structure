﻿using Homework3.DAL.Entities;
using Newtonsoft.Json;

namespace Homework3.Common.DTOs
{
    public class UserDataStructureDto
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("lastProject")]
        public Project LastProject { get; set; }

        [JsonProperty("tasksCount")]
        public int TasksCount { get; set; }

        [JsonProperty("notFinishedTasksCount")]
        public int NotFinishedTasksCount { get; set; }

        [JsonProperty("longestTask")]
        public Task LongestTask { get; set; }

        public UserDataStructureDto(User user, Project lastProject, int tasksCount, int notFinishedTasksCount, Task task)
        {
            User = user;
            LastProject = lastProject;
            TasksCount = tasksCount;
            NotFinishedTasksCount = notFinishedTasksCount;
            LongestTask = task;
        }
    }
}
