﻿using Homework3.DAL.Entities;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Homework3.Common.DTOs
{
    public class TeamsMembersDto
    {
        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        [JsonProperty("teamName")]
        public string TeamName { get; set; }

        [JsonProperty("users")]
        public List<User> Users { get; set; } = new List<User>();

        public TeamsMembersDto(int? id, string name)
        {
            TeamId = id;
            TeamName = name;
        }

    }
}
