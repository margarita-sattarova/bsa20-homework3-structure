﻿using Homework3.DAL.Entities;
using Newtonsoft.Json;

namespace Homework3.Common.DTOs
{
    public class ProjectDataStructureDto
    {
        [JsonProperty("project")]
        public Project Project { get; set; }

        [JsonProperty("taskWithLongestDescription")]
        public Task TaskWithLongestDescription { get; set; }

        [JsonProperty("taskWithShortestName")]
        public Task taskWithShortestName { get; set; }

        [JsonProperty("teamMembersCount")]
        public int TeamMembersCount { get; set; }

        public ProjectDataStructureDto(Project project, Task task1, Task task2)
        {
            Project = project;
            TaskWithLongestDescription = task1;
            taskWithShortestName = task2;
        }
    }
}
