﻿using Homework3.DAL.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Homework3.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static string DataFilesPath { get; set; } = Path.GetFullPath(@"..\Homework3.DAL\Context");

        public static ICollection<Project> LoadProjectsFromJson(string filePath)
        {
            using StreamReader r = new StreamReader(filePath);
            string json = r.ReadToEnd();
            List<Project> items = JsonConvert.DeserializeObject<List<Project>>(json);

            return items;
        }

        public static ICollection<Task> LoadTasksFromJson(string filePath)
        {
            using StreamReader r = new StreamReader(filePath);
            string json = r.ReadToEnd();
            List<Task> items = JsonConvert.DeserializeObject<List<Task>>(json);

            return items;
        }

        public static ICollection<Team> LoadTeamsFromJson(string filePath)
        {
            using StreamReader r = new StreamReader(filePath);
            string json = r.ReadToEnd();
            List<Team> items = JsonConvert.DeserializeObject<List<Team>>(json);

            return items;
        }

        public static ICollection<User> LoadUsersFromJson(string filePath)
        {
            using StreamReader r = new StreamReader(filePath);
            string json = r.ReadToEnd();
            List<User> items = JsonConvert.DeserializeObject<List<User>>(json);

            return items;
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasData(DataSeed.Projects);
            modelBuilder.Entity<User>().HasData(DataSeed.Users);
            modelBuilder.Entity<Task>().HasData(DataSeed.Tasks);
            modelBuilder.Entity<Team>().HasData(DataSeed.Teams);
        }

        public static void Configure(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Project>()
                .Property(pr => pr.Name)
                .IsRequired();

            modelBuilder.Entity<Project>()
                .HasMany(pr => pr.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(t => t.ProjectId);

            modelBuilder.Entity<Project>()
                .HasOne(u => u.Author)
                .WithMany()
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Project>()
                .HasOne(u => u.Team)
                .WithMany()
                .HasForeignKey(p => p.TeamId);

            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                .IsRequired();

            modelBuilder.Entity<Task>()
                .HasOne(u => u.Performer)
                .WithMany()
                .HasForeignKey(t => t.PerformerId);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Project)
                .WithMany()
                .HasForeignKey(t => t.ProjectId);

            modelBuilder.Entity<Team>()
                .Property(t => t.Name)
                .IsRequired();

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Members)
                .WithOne(u => u.Team)
                .HasForeignKey(u => u.TeamId);

            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Tasks)
                .WithOne(t => t.Performer)
                .HasForeignKey(t => t.PerformerId);
        }
    }
}
