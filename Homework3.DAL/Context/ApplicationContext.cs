﻿using Homework3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Homework3.DAL.Context
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base (options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            modelBuilder.Seed();
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Entities.Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<TEntity> SetOf<TEntity>() where TEntity : class
        {
            if (Projects is IEnumerable<TEntity>)
            {
                Tasks.Load();
                Teams.Load();
                Users.Load();

                return Projects as DbSet<TEntity>;
            }
            else if (Tasks is IEnumerable<TEntity>)
            {
                Users.Load();
                Projects.Load();
                Teams.Load();

                return Tasks as DbSet<TEntity>;
            }
            else if (Teams is IEnumerable<TEntity>)
            {
                Users.Load();
                Projects.Load();
                Tasks.Load();

                return Teams as DbSet<TEntity>;
            }
            else
            {
                Projects.Load();
                Tasks.Load();
                Teams.Load();

                return Users as DbSet<TEntity>;
            }
        }
    }
}
