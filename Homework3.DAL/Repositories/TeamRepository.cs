﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.DAL.Repositories
{
    public class TeamRepository : BaseRepository<Team>
    {
        public TeamRepository(ApplicationContext db) : base(db)
        {

        }

        public override async System.Threading.Tasks.Task Update(Team entity, int id)
        {
            var entityToUpdate = await DbContext.SetOf<Team>().SingleOrDefaultAsync(x => x.Id == id);

            if (entityToUpdate != null)
            {
                entityToUpdate.Name = entity.Name;

                DbContext.Teams.Update(entityToUpdate);
                await base.Update(entity, id);
            }            
        }
    }
}
