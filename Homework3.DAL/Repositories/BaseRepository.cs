﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.DAL.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationContext DbContext;

        public BaseRepository(ApplicationContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual async System.Threading.Tasks.Task Create(TEntity entity) 
        {
            DbContext.SetOf<TEntity>().Add(entity);
            await DbContext.SaveChangesAsync();
        }

        public virtual async System.Threading.Tasks.Task Delete(TEntity entity)
        {
            DbContext.SetOf<TEntity>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }

        public virtual async System.Threading.Tasks.Task Delete(int id)
        {
            var itemToDelete = await DbContext.SetOf<TEntity>().SingleOrDefaultAsync(x => x.Id == id);
            //await DbContext.SetOf<TEntity>().Remove(DbContext.SetOf<TEntity>().SingleOrDefaultAsync(x => x.Id == id));
            DbContext.SetOf<TEntity>().Remove(itemToDelete);
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task<List<TEntity>> Get()
        {
            return await DbContext.SetOf<TEntity>().ToListAsync();
        }

        public virtual async Task<TEntity> Get(int id)
        {
            return await DbContext.SetOf<TEntity>().Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public virtual async System.Threading.Tasks.Task Update(TEntity entity, int id)
        {
            await DbContext.SaveChangesAsync();
        }
    }
}
