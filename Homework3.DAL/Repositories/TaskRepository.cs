﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.DAL.Repositories
{
    public class TaskRepository : BaseRepository<Entities.Task>
    {
        public TaskRepository(ApplicationContext db) : base(db)
        {

        }

        public override async System.Threading.Tasks.Task Update(Entities.Task entity, int id)
        {
            var entityToUpdate = await DbContext.SetOf<Entities.Task>().SingleOrDefaultAsync(x => x.Id == id);

            if (entityToUpdate != null)
            {
                if (entity.State == 2 && entityToUpdate.State != 2)
                {
                    entityToUpdate.FinishedAt = DateTime.Now;
                }

                entityToUpdate.PerformerId = entity.PerformerId;
                entityToUpdate.Name = entity.Name;
                entityToUpdate.Description = entity.Description;
                entityToUpdate.State = entity.State;
                entityToUpdate.ProjectId = entity.ProjectId;

                DbContext.Tasks.Update(entityToUpdate);
                await base.Update(entity, id);
            }
        }
    }
}
