﻿using Homework3.DAL.Entities;
using Homework3.DAL.Context;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Homework3.DAL.Repositories
{
    public class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(ApplicationContext db) : base(db)
        {

        }

        public override async System.Threading.Tasks.Task Update(Project entity, int id)
        {
            var entityToUpdate = await DbContext.SetOf<Project>().SingleOrDefaultAsync(x => x.Id == id);

            if (entityToUpdate != null)
            {
                entityToUpdate.Name = entity.Name;
                entityToUpdate.Description = entity.Description;
                entityToUpdate.TeamId = entity.TeamId;
                entityToUpdate.Deadline = entity.Deadline;

                DbContext.Projects.Update(entityToUpdate);
                await base.Update(entity, id);
            }
        }
    }
}
