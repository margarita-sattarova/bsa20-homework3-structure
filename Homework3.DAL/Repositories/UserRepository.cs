﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(ApplicationContext db) : base(db)
        {

        }

        public override async System.Threading.Tasks.Task Update(User entity, int id)
        {
            var searchedItem = await DbContext.Teams.FindAsync(entity.TeamId);
            if (searchedItem == null) throw new ArgumentException("Team with Id provided in property 'TeamId' does not exist.", "entity");

            var entityToUpdate = await DbContext.SetOf<User>().SingleOrDefaultAsync(x => x.Id == id);

            if (entityToUpdate != null)
            {
                entityToUpdate.FirstName = entity.FirstName;
                entityToUpdate.LastName = entity.LastName;
                entityToUpdate.Birthday = entity.Birthday;
                entityToUpdate.Email = entity.Email;
                entityToUpdate.TeamId = entity.TeamId;

                DbContext.Users.Update(entityToUpdate);
                await base.Update(entity, id);
            }
        }
    }
}
