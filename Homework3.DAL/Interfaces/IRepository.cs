﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Homework3.DAL.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<List<TEntity>> Get();

        Task<TEntity> Get(int id);

        System.Threading.Tasks.Task Create(TEntity entity);

        System.Threading.Tasks.Task Update(TEntity entity, int id);

        System.Threading.Tasks.Task Delete(TEntity entity);

        System.Threading.Tasks.Task Delete(int id);
    }
}
