﻿using Homework3.DAL.Entities;
using System.Threading.Tasks;

namespace Homework3.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<Entities.Task> Tasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}
