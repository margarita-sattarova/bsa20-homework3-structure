﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Homework3.DAL.Entities
{
    public class Team : BaseEntity
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public ICollection<User> Members { get; set; }
    }
}
