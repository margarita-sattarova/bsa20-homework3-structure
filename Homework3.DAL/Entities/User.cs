﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Homework3.DAL.Entities
{
    public class User : BaseEntity
    {

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }

        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }

        [JsonIgnore]
        public ICollection<Task> Tasks { get; set; }
    }
}
