﻿using System;
using Newtonsoft.Json;

namespace Homework3.DAL.Entities
{
    public class Task : BaseEntity
    {
        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public int State { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonIgnore]
        public Project Project { get; set; }

        [JsonIgnore]
        public User Performer { get; set; }
    }
}
