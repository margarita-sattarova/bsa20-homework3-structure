﻿using Newtonsoft.Json;

namespace Homework3.DAL.Entities
{
    public class BaseEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
