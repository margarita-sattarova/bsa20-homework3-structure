﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Homework3.DAL.Entities
{
    public class Project : BaseEntity
    {
        public List<Task> Tasks { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonIgnore]
        public User Author { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }
    }
}
