﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using Homework3.DAL.Interfaces;
using Homework3.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace Homework3.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(_context);
                return _projectRepository;
            }
        }

        public IRepository<Entities.Task> Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository(_context);
                return _taskRepository;
            }
        }

        public IRepository<Team> Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(_context);
                return _teamRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }
    }
}
