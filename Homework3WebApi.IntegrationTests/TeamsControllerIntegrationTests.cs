﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using System;
using System.Net.Http;
using Xunit;
using Newtonsoft.Json;
using System.Text;
using System.Net;

namespace Homework3WebApi.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly ApplicationContext context;

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            context = factory.GetContext();
            _client = factory.CreateClient();

        }

        public void Dispose()
        {

        }

        [Fact(DisplayName = "Create Team (Valid request body)")]
        public async System.Threading.Tasks.Task Post_WhenBodyIsValid_ThenResponseWithCode201AndCorrespondedBody()
        {
            var team = new Team() { Name = "NewTeam", CreatedAt = DateTime.Now };
            var jsonInString = JsonConvert.SerializeObject(team);

            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResp = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<Team>(stringResp);

            await _client.DeleteAsync($"api/teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(team.Name, createdTeam.Name);
            Assert.Equal(team.CreatedAt, createdTeam.CreatedAt);
        }

        [Fact(DisplayName = "Create Team (Invalid request body)")]
        public async System.Threading.Tasks.Task Post_WhenTaskBodyIsInvalid_ThenResponseWithCode400()
        {
            var team = new Team() { Name = "", CreatedAt = DateTime.Now };
            var jsonInString = JsonConvert.SerializeObject(team);

            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
