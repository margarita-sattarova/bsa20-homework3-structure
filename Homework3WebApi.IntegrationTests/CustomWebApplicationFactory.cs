﻿using Homework3.DAL.Context;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using System;

namespace Homework3WebApi.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        public ApplicationContext GetContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;

            var context = new ApplicationContext(options);

            return context;
        }
    }
}
