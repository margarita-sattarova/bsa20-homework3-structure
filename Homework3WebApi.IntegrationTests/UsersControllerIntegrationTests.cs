using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using System;
using System.Net.Http;
using Xunit;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Collections.Generic;

namespace Homework3WebApi.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly ApplicationContext context;

        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
             context = factory.GetContext();
            _client = factory.CreateClient();
            
        }

        public void Dispose()
        {
           
        }

        [Fact(DisplayName = "Delete User (Valid request body)")]
        public async System.Threading.Tasks.Task Delete_WhenUserIdIsValid_ThenResponseWithCode204AndTasksSetDoesNotContainDeletedTask()
        {
            var user = new User() { FirstName = "First name", LastName = "Last name", Email = "someemail@gmail.com", Birthday = new DateTime(2001, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), RegisteredAt = new DateTime(2019, 1, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(5000) };
            var jsonInString = JsonConvert.SerializeObject(user);
            var httpPostResponse = await _client.PostAsync("api/users", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringPostResp = await httpPostResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<User>(stringPostResp);

            var httpDeleteResponse = await _client.DeleteAsync($"api/users/{createdUser.Id}");
            var stringGetTasksResp = await _client.GetAsync($"api/users").Result.Content.ReadAsStringAsync();
            var usersList = JsonConvert.DeserializeObject<List<User>>(stringGetTasksResp);

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponse.StatusCode);
            Assert.DoesNotContain(createdUser, usersList);
        }

        [Theory(DisplayName = "Delete User (Invalid request body)")]
        [InlineData(130)]
        public async System.Threading.Tasks.Task Delete_WhenUserIdIsInvalid_ThenResponseWithCode404(int userId)
        {
            var httpDeleteResponse = await _client.DeleteAsync($"api/users/{userId}");

            Assert.Equal(HttpStatusCode.NotFound, httpDeleteResponse.StatusCode);
        }
    }
}
