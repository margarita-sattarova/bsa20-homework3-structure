﻿using Homework3.DAL.Context;
using Homework3.DAL.Entities;
using System;
using System.Net.Http;
using Xunit;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Collections.Generic;
using Homework3.Common;
using System.Linq;

namespace Homework3WebApi.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly ApplicationContext context;

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            context = factory.GetContext();
            _client = factory.CreateClient();

        }

        public void Dispose()
        {

        }

        [Fact(DisplayName = "Create Project (Valid request body)")]
        public async System.Threading.Tasks.Task Post_WhenProjectBodyIsValid_ThenResponseWithCode201AndCorrespondedBody()
        {
            var project = new Project() { Name = "NewProj", Description = "NewProjDescription", AuthorId = 5, Deadline = new DateTime(2021, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), CreatedAt = DateTime.Now };
            var jsonInString = JsonConvert.SerializeObject(project);

            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResp = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<Project>(stringResp);

            await _client.DeleteAsync($"api/projects/{createdProject.Id}");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(project.Name, createdProject.Name);
            Assert.Equal(project.Description, createdProject.Description);
            Assert.Equal(project.AuthorId, createdProject.AuthorId);
        }

        [Theory(DisplayName = "Create Project (Invalid request body)")]
        [InlineData("", "Description", 2)]
        [InlineData("Name", "", 11)]
        [InlineData("Name", "Description", 117)]
        public async System.Threading.Tasks.Task Post_WhenProjectBodyIsInvalid_ThenResponseWithCode400(string name, string description, int authorId)
        {
            var project = new Project() { Name = name, Description = description, AuthorId = authorId, Deadline = new DateTime(2022, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), CreatedAt = DateTime.Now };
            var jsonInString = JsonConvert.SerializeObject(project);

            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Theory]
        [InlineData(4, new int[] { 16, 33 }, new int[] { 3, 1 })]
        public async System.Threading.Tasks.Task GetNumberOfTasksOnUsersProjects_WhenUserIdIsValid_ThenResponseWithCode200AndReturnExpectedResult(int userId, int[] projectsId, int[] amountsOfTasks)
        {
            var httpGetResponse = await _client.GetAsync($"api/projects/tasks/{userId}");
            var stringGetResponse = await httpGetResponse.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<UserProjectsTasksDto>>(stringGetResponse);
            var actualProjectsId = resultList.Select(x => x.ProjectId).ToArray();
            var actualAmountsOfTasks = resultList.Select(x => x.TasksCount).ToArray();

            Assert.Equal(HttpStatusCode.OK, httpGetResponse.StatusCode);
            Assert.Equal(projectsId, actualProjectsId);
            Assert.Equal(amountsOfTasks, actualAmountsOfTasks);
        }

        [Theory]
        [InlineData(78)]
        public async System.Threading.Tasks.Task GetNumberOfTasksOnUsersProjects_WhenUserIdIsValid_ThenResponseWithCode404(int userId)
        {
            var httpGetResponse = await _client.GetAsync($"api/projects/tasks/{userId}");

            Assert.Equal(HttpStatusCode.NotFound, httpGetResponse.StatusCode);
        }
    }
}
