﻿using Homework3.DAL.Context;
using System;
using System.Net.Http;
using Xunit;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Linq;

namespace Homework3WebApi.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly ApplicationContext context;

        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            context = factory.GetContext();
            _client = factory.CreateClient();

        }

        public void Dispose()
        {

        }

        [Fact(DisplayName = "Delete Task (Valid request body)")]
        public async System.Threading.Tasks.Task Delete_WhenTaskIdIsValid_ThenResponseWithCode204AndTasksSetDoesNotContainDeletedTask()
        {
            var task = new Homework3.DAL.Entities.Task() { Name = "TaskName", Description = "TaskDescription", PerformerId = 17, ProjectId = 3, FinishedAt = new DateTime(2021, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), CreatedAt = new DateTime(2021, 1, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(5000), State = 2 };
            var jsonInString = JsonConvert.SerializeObject(task);
            var httpPostResponse = await _client.PostAsync("api/tasks", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringPostResp = await httpPostResponse.Content.ReadAsStringAsync();
            var createdTask = JsonConvert.DeserializeObject<Homework3.DAL.Entities.Task>(stringPostResp);

            var httpDeleteResponse = await _client.DeleteAsync($"api/tasks/{createdTask.Id}");
            var stringGetTasksResp = await _client.GetAsync($"api/tasks").Result.Content.ReadAsStringAsync();
            var tasksList = JsonConvert.DeserializeObject<List<Homework3.DAL.Entities.Task>>(stringGetTasksResp);

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponse.StatusCode);
            Assert.DoesNotContain(createdTask, tasksList);
        }

        [Theory(DisplayName = "Delete Task (Invalid request body)")]
        [InlineData(570)]
        public async System.Threading.Tasks.Task Delete_WhenTaskIdIsInvalid_ThenResponseWithCode404(int taskId)
        {
            var httpDeleteResponse = await _client.DeleteAsync($"api/tasks/{taskId}");

            Assert.Equal(HttpStatusCode.NotFound, httpDeleteResponse.StatusCode);
        }

        [Theory]
        [InlineData(13, new int[] { 92, 114, 139, 145, 150, 192 })]
        [InlineData(18, new int[] { 15, 125 })]
        public async System.Threading.Tasks.Task GetUserNotFinishedTasks_WhenUserIdIsValid_ThenResponseWithCode200AndReturnExpectedResult(int userId, int[] tasksId)
        {
            var httpDeleteResponse = await _client.GetAsync($"api/tasks/user/notFinished/{userId}");
            var stringGetResponse = await httpDeleteResponse.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<Homework3.DAL.Entities.Task>>(stringGetResponse);
            var actualTasksId = resultList.Select(x => x.Id).ToArray();

            Assert.Equal(HttpStatusCode.OK, httpDeleteResponse.StatusCode);
            Assert.Equal(tasksId, actualTasksId);
        }

        [Theory]
        [InlineData(0)]
        public async System.Threading.Tasks.Task GetUserNotFinishedTasks_WhenUserIdIsValid_ThenResponseWithCode404(int userId)
        {
            var httpDeleteResponse = await _client.GetAsync($"api/tasks/user/notFinished/{userId}");

            Assert.Equal(HttpStatusCode.NotFound, httpDeleteResponse.StatusCode);
        }
    }
}
