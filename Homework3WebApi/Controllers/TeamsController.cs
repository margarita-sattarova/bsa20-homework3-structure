﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Homework3.BLL.Interfaces;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Homework3WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : Controller
    {
        readonly ITeamService _service;

        public TeamsController(ITeamService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<List<Project>>> Get()
        {
            return Ok(await _service.GetTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> Get(int id)
        {
            return Ok(await _service.GetTeamById(id));
        }

        [HttpGet("membersOlder10")]
        public async Task<ActionResult<List<TeamsMembersDto>>> GetTeamMembersOlder10()
        {
            return Ok(await _service.GetTeamMembersOlder10());
        }

        [HttpPost]
        public async Task<ActionResult<Team>> Post([FromBody] Team value)
        {
            try
            {
                await _service.Create(value);
                return Created($"{Request.GetDisplayUrl()}/{value.Id}", value);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Team value)
        {
            await _service.Update(value, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _service.Delete(Convert.ToInt32(id));
            return NoContent();
        }
    }
}
