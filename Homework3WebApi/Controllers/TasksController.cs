﻿using System;
using System.Collections.Generic;
using Homework3.BLL.Interfaces;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Homework3.DAL.Entities;
using System.Threading.Tasks;

namespace Homework3WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : Controller
    {
        readonly ITasksService _service;

        public TasksController(ITasksService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<List<Homework3.DAL.Entities.Task>>> Get()
        {
            return Ok(await _service.GetTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Homework3.DAL.Entities.Task>> Get(int id)
        {
            return Ok(await _service.GetTaskById(id));
        }

        [HttpGet("user/{id}")]
        public async Task<ActionResult<List<Homework3.DAL.Entities.Task>>> GetUserTasksById(string id)
        {
            return Ok(await _service.GetUserTasks(Convert.ToInt32(id)));
        }

        [HttpGet("user/notFinished/{id}")]
        public async Task<ActionResult<List<Homework3.DAL.Entities.Task>>> GetUserNotFinishedTasksById(string id)
        {
            try
            {
                return Ok(await _service.GetUserNotFinishedTasks(Convert.ToInt32(id)));
            }
            catch (Exception)
            {
                return NotFound();
            }            
        }

        [HttpGet("user/finishedIn2020/{id}")]
        public async Task<ActionResult<List<Homework3.DAL.Entities.Task>>> GetUserTasksFinishedIn2020(string id)
        {
            return Ok(await _service.GetUserTasksFinishedIn2020(Convert.ToInt32(id)));
        }

        [HttpPost]
        public async Task<ActionResult<Homework3.DAL.Entities.Task>> Post([FromBody] Homework3.DAL.Entities.Task value)
        {
            await _service.Create(value);
            return Created($"{Request.GetDisplayUrl()}/{value.Id}", value);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Homework3.DAL.Entities.Task value)
        {
            await _service.Update(value, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _service.Delete(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
    }
}
