﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Homework3.BLL.Interfaces;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Homework3WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        readonly IUserService _service;

        public UsersController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {
            return Ok(await _service.GetUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            return Ok(await _service.GetUserById(id));
        }

        [HttpGet("tasksSorted")]
        public async Task<ActionResult<List<UserWithTasksSortedDto>>> GetUsersWithTasksSorted()
        {
            return Ok(await _service.GetUsersWithTasksSorted());
        }

        [HttpGet("userDataStruct/{id}")]
        public async Task<ActionResult<UserDataStructureDto>> GetUserDataStrusture(int id)
        {
            return Ok(await _service.GetUserDataStrusture(id));
        }

        [HttpPost]
        public async Task<ActionResult<User>> Post([FromBody] User value)
        {
            await _service.Create(value);
            return Created($"{Request.GetDisplayUrl()}/{value.Id}", value);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] User value)
        {
            await _service.Update(value, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _service.Delete(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
    }
}