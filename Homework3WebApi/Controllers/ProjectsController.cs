﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Homework3.BLL.Interfaces;
using Homework3.Common;
using Homework3.Common.DTOs;
using Homework3.DAL.Entities;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Homework3WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : Controller
    {
        readonly IProjectService _service;

        public ProjectsController(IProjectService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<List<Project>>> Get()
        {
            return Ok(await _service.GetProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> Get(int id)
        {
            return Ok(await _service.GetProjectById(id));
        }

        [HttpGet("tasks/{id}")]
        public async Task<ActionResult<List<UserProjectsTasksDto>>> GetProjectTasks(string id)
        {
            try
            {
                return Ok(await _service.GetNumberOfTasksOnUsersProjects(Convert.ToInt32(id)));
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
                   
        }

        [HttpGet("projectsDataStructure")]
        public async Task<ActionResult<List<ProjectDataStructureDto>>> GetProjectsDataStructure()
        {
            return Ok(await _service.GetProjectsDataStructure());
        }

        [HttpPost]
        public async Task<ActionResult<Project>> Post([FromBody] Project value)
        {
            try
            {
                await _service.Create(value);
                return Created($"{Request.GetDisplayUrl()}/{value.Id}", value);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Project value)
        {
            await _service.Update(value, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _service.Delete(Convert.ToInt32(id));
            return NoContent();
        }
    }
}
