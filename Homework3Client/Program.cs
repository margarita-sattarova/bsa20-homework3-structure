﻿using Homework3.DAL.Context;
using System;
using Homework3Client.MenuProvider;
using System.Threading.Tasks;

namespace Homework3Client
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (MenuProvider.MenuProvider.toContinue)
            {
                MenuProvider.MenuProvider.ProvideMenu();

                Console.ReadLine();
            }

            Console.ReadKey();
        }
    }
}
