﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Homework3.DAL.Entities;
using Homework3.Common;
using Homework3.Common.DTOs;
using System.Threading.Tasks;

namespace Homework3Client.MenuProvider
{
    public static class MenuProvider
    {
        static HttpClient client = new HttpClient();
        public static bool toContinue { get; private set; } = true;
        private static Func<System.Threading.Tasks.Task>[] MenuActions;
        readonly static string MenuOptions;

        static MenuProvider()
        {
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            MenuOptions = "Choose an action: \n" +
                "0. Exit \n" +
                "1. Get projects \n" +
                "2. Get project by Id \n" +
                "3. Create project \n" +
                "4. Delete project \n" +
                "5. Update project \n" +
                "6. Get tasks \n" +
                "7. Get task by Id \n" +
                "8. Create task \n" +
                "9. Delete task \n" +
                "10. Update task \n" +
                "11. Get teams \n" +
                "12. Get team by Id \n" +
                "13. Create team \n" +
                "14. Delete team \n" +
                "15. Update team \n" +
                "16. Get users \n" +
                "17. Get user by Id \n" +
                "18. Create user \n" +
                "19. Delete user \n" +
                "20. Update user \n" +
                "21. Get user's tasks \n" +
                "22. Get user's tasks finished in 2020 \n" +
                "23. Get team members older 10 \n" +
                "24. Get number of tasks on user's projects \n" +
                "25. Get users with tasks sorted \n" +
                "26. Get user data structure \n" +
                "27. Get project data structure \n" +
                "28. Get all user's not finished tasks \n";

            MenuActions = new Func<System.Threading.Tasks.Task>[] { Exit, GetProjects, GetProjectById, CreateProject, DeleteProject, UpdateProject, GetTasks, GetTaskById, 
                CreateTask, DeleteTask, UpdateTask, GetTeams, GetTeamById, CreateTeam, DeleteTeam, UpdateTeam, GetUsers, GetUserById, CreateUser, DeleteUser, UpdateUser, GetUserTasks, 
                  GetFinishedTasksIn2020, GetTeamMembersOlder10, GetNumberOfTasksOnUsersProject, GetUsersWithTasksSorted, GetUserDataStrusture, GetProjectsDataStructure, GetUserNotFinishedTasks };
        }
        public static async System.Threading.Tasks.Task ProvideMenu()
        {
            Console.WriteLine(MenuOptions);
            Console.Write("Your input : ");

            try
            {
                var userInput = Convert.ToInt32(Console.ReadLine());
                await MenuActions[userInput]();
                Console.WriteLine();
            }
            catch (FormatException)
            {
                Console.WriteLine("Your input is not a number.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Error. Please enter a number between 0 and {MenuActions.Length}.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

        }

        private static async System.Threading.Tasks.Task Exit()
        {
            toContinue = false;
        }

        private static async System.Threading.Tasks.Task GetProjects()
        {
            var response = await client.GetAsync("api/projects");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            List<Project> projList = await response.Content.ReadAsAsync<List<Project>>();

            Console.WriteLine("Projects: ");

            foreach(var item in projList)
            {
                Console.WriteLine($"Project Id : {item.Id}  |  Name : {item.Name}\nDescription : {item.Description}");
            }
        }

        private static async System.Threading.Tasks.Task GetTasks()
        {
            var response = await client.GetAsync("api/tasks");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            List<Homework3.DAL.Entities.Task> tasksList = await response.Content.ReadAsAsync<List<Homework3.DAL.Entities.Task>>();

            Console.WriteLine("Tasks: ");

            foreach (var item in tasksList)
            {
                Console.WriteLine($"Task Id : {item.Id}  |  Task Name : {item.Name}\nDescription : {item.Description}");
            }

        }

        private static async System.Threading.Tasks.Task GetTeams()
        {
            var response = await client.GetAsync("api/teams");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            List<Team> teamsList = await response.Content.ReadAsAsync<List<Team>>();

            Console.WriteLine("Teams: ");

            foreach (var item in teamsList)
            {
                Console.WriteLine($"Team Id : {item.Id}  |  Team Name : {item.Name}  |  Created : {item.CreatedAt}");
            }

        }

        private static async System.Threading.Tasks.Task GetUsers()
        {
            var response = await client.GetAsync("api/Users");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            List<User> teamsList = await response.Content.ReadAsAsync<List<User>>();

            Console.WriteLine("Users: ");

            foreach (var item in teamsList)
            {
                Console.WriteLine($"User Id : {item.Id}  |  User Name : {item.FirstName} {item.LastName}  |  Email : {item.Email}\nRegistered : {item.RegisteredAt}");
            }

        }

        private static async System.Threading.Tasks.Task GetProjectById()
        {
            try
            {
                Console.Write("Enter project ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/projects/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                Project searchedProject = await response.Content.ReadAsAsync<Project>();

                Console.WriteLine("Project info: ");

                    Console.WriteLine($"Id : {searchedProject.Id} \nName : {searchedProject.Name} \nDescription : {searchedProject.Description} \nCreated : {searchedProject.CreatedAt} \nDeadline : {searchedProject.Deadline}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetTaskById()
        {
            try
            {
                Console.Write("Enter task ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/tasks/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                Homework3.DAL.Entities.Task searchedTask = await response.Content.ReadAsAsync<Homework3.DAL.Entities.Task>();

                Console.WriteLine("Task info: ");

                Console.WriteLine($"Id : {searchedTask.Id} \nName : {searchedTask.Name} \nDescription : {searchedTask.Description} \nCreated : {searchedTask.CreatedAt} \nState : {searchedTask.State}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetTeamById()
        {
            try
            {
                Console.Write("Enter team ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/teams/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                Team searchedTeam = await response.Content.ReadAsAsync<Team>();

                Console.WriteLine("Team info: ");

                Console.WriteLine($"Id : {searchedTeam.Id} \nName : {searchedTeam.Name}  \nCreated : {searchedTeam.CreatedAt}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetUserById()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/users/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                User searchedUser = await response.Content.ReadAsAsync<User>();

                Console.WriteLine("User info: ");

                Console.WriteLine($"Id : {searchedUser.Id} \nName : {searchedUser.FirstName} {searchedUser.LastName} \nEmail : {searchedUser.Email} \nRegistered : {searchedUser.RegisteredAt}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task CreateProject()
        {
            Project addedProject;
            try
            {
                Console.Write("Enter project name  : ");
                var name = Console.ReadLine();

                Console.Write("Enter project description  : ");
                var description = Console.ReadLine();

                Console.Write("Enter project's author Id  : ");
                var authorId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter project's team Id  : ");
                var teamId = Convert.ToInt32(Console.ReadLine());


                Console.Write("Enter project's deadline  : ");
                var deadline = Convert.ToDateTime(Console.ReadLine());

                addedProject = new Project() { Name = name, Description = description, AuthorId = authorId, TeamId = teamId, Deadline = deadline};

                var response = await client.PostAsJsonAsync("api/projects", addedProject);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't add project. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Project added");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task CreateTask()
        {
            Homework3.DAL.Entities.Task addedTask;
            try
            {
                Console.Write("Enter task name  : ");
                var name = Console.ReadLine();

                Console.Write("Enter task description  : ");
                var description = Console.ReadLine();

                Console.Write("Enter task's performer Id  : ");
                var performerId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter task's project Id  : ");
                var projectId = Convert.ToInt32(Console.ReadLine());

                addedTask = new Homework3.DAL.Entities.Task() { Name = name, Description = description, PerformerId = performerId, ProjectId = projectId };

                var response = await client.PostAsJsonAsync("api/tasks", addedTask);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't add task. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Task added");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static async System.Threading.Tasks.Task CreateTeam()
        {
            Team addedTeam;
            try
            {
                Console.Write("Enter team name  : ");
                var name = Console.ReadLine();

                addedTeam = new Team() { Name = name};

                var response = await client.PostAsJsonAsync("api/teams", addedTeam);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't add team. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Team added");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task CreateUser()
        {
            User addedUser;
            try
            {
                Console.Write("Enter first name  : ");
                var firstName = Console.ReadLine();

                Console.Write("Enter last name : ");
                var lastName = Console.ReadLine();

                Console.Write("Enter email : ");
                var email = Console.ReadLine();

                Console.Write("Enter birthdate : ");
                var birthdate = Convert.ToDateTime(Console.ReadLine());

                addedUser = new User() { FirstName = firstName, LastName = lastName, Email = email, Birthday = birthdate };

                var response = await client.PostAsJsonAsync("api/users", addedUser);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't add user. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("User added");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task DeleteProject()
        {
            Console.Write("Enter project Id : ");
            var id = Console.ReadLine();
            var response = await client.DeleteAsync($"api/projects/{id}");
            Console.WriteLine($"Status code: {response.StatusCode}");
        }

        private static async System.Threading.Tasks.Task DeleteTask()
        {
            Console.Write("Enter task Id : ");
            var id = Console.ReadLine();
            var response = await client.DeleteAsync($"api/tasks/{id}");
            Console.WriteLine($"Status code: {response.StatusCode}");
        }

        private static async System.Threading.Tasks.Task DeleteTeam()
        {
            Console.Write("Enter team Id : ");
            var id = Console.ReadLine();
            var response = await client.DeleteAsync($"api/teams/{id}");
            Console.WriteLine($"Status code: {response.StatusCode}");
        }

        private static async System.Threading.Tasks.Task DeleteUser()
        {
            Console.Write("Enter user Id : ");
            var id = Console.ReadLine();
            var response = await client.DeleteAsync($"api/users/{id}");
            Console.WriteLine($"Status code: {response.StatusCode}");
        }

        private static async System.Threading.Tasks.Task UpdateProject()
        {
            Project newProjectInfo;
            try
            {
                Console.Write("Enter project Id  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new project name  : ");
                var newName = Console.ReadLine();

                Console.Write("Enter new project description  : ");
                var newDescription = Console.ReadLine();

                Console.Write("Enter new project's team Id  : ");
                var newTeamId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new project deadline  : ");
                var newDeadline = Convert.ToDateTime(Console.ReadLine());

                newProjectInfo = new Project() { Name = newName, Description = newDescription, TeamId = newTeamId, Deadline = newDeadline };

                var response = await client.PutAsJsonAsync($"api/projects/{id}", newProjectInfo);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't update project. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Project updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task UpdateTask()
        {
            Homework3.DAL.Entities.Task newTaskInfo;
            try
            {
                Console.Write("Enter task Id  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new task name  : ");
                var newName = Console.ReadLine();

                Console.Write("Enter new task description  : ");
                var newDescription = Console.ReadLine();

                Console.Write("Enter new task's project Id  : ");
                var newTaskProjectId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new task's performer Id  : ");
                var newTaskPerformerId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new task state  : ");
                var newTaskState = Convert.ToInt32(Console.ReadLine());

                newTaskInfo = new Homework3.DAL.Entities.Task() { Name = newName, Description = newDescription, ProjectId = newTaskProjectId, PerformerId = newTaskPerformerId, State = newTaskState };

                var response = await client.PutAsJsonAsync($"api/tasks/{id}", newTaskInfo);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't update task. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Task updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task UpdateTeam()
        {
            Team newTeamInfo;
            try
            {
                Console.Write("Enter team Id  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new team name  : ");
                var newName = Console.ReadLine();

                newTeamInfo = new Team() { Name = newName };

                var response = await client.PutAsJsonAsync($"api/teams/{id}", newTeamInfo);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't update team. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Team updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task UpdateUser()
        {
            User newUserInfo;
            try
            {
                Console.Write("Enter user Id  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter new first name  : ");
                var newFirstName = Console.ReadLine();

                Console.Write("Enter new last name  : ");
                var newLastName = Console.ReadLine();

                Console.Write("Enter new date of birth  : ");
                var newBirthDate = Convert.ToDateTime(Console.ReadLine());

                Console.Write("Enter new email  : ");
                var newEmail = Console.ReadLine();

                Console.Write("Enter user's team Id  : ");
                var newTeamId = Convert.ToInt32(Console.ReadLine());

                newUserInfo = new User() { FirstName = newFirstName, LastName = newLastName, Birthday = newBirthDate, Email = newEmail, TeamId = newTeamId };

                var response = await client.PutAsJsonAsync($"api/users/{id}", newUserInfo);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't update user profile. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("User profile updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetUserTasks()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/tasks/user/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                List<Homework3.DAL.Entities.Task> tasksList = await response.Content.ReadAsAsync<List<Homework3.DAL.Entities.Task>>();

                Console.WriteLine("Tasks: ");

                foreach (var item in tasksList)
                {
                    Console.WriteLine($"Id: {item.Id} \nName: {item.Name} \nDescription: {item.Description}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetUserNotFinishedTasks()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/tasks/user/notFinished/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                List<Homework3.DAL.Entities.Task> tasksList = await response.Content.ReadAsAsync<List<Homework3.DAL.Entities.Task>>();

                Console.WriteLine("Tasks: ");

                foreach (var item in tasksList)
                {
                    Console.WriteLine($"Id: {item.Id} \nName: {item.Name} \nDescription: {item.Description} \nTask State: {item.State}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetFinishedTasksIn2020()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/tasks/user/finishedIn2020/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                List<Homework3.DAL.Entities.Task> tasksList = await response.Content.ReadAsAsync<List<Homework3.DAL.Entities.Task>>();

                Console.WriteLine("Tasks finished in 2020: ");

                foreach (var item in tasksList)
                {
                    Console.WriteLine($"Id: {item.Id} \nName: {item.Name} \nFinished: {item.FinishedAt}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetTeamMembersOlder10()
        {
            try
            {
                var response = await client.GetAsync($"api/teams/membersOlder10");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                Console.WriteLine("Teams' members older 10: ");

                var list = await response.Content.ReadAsAsync<List<TeamsMembersDto>>();

                foreach(var item in list)
                {
                    Console.WriteLine($"Team Id : {item.TeamId}  Team name : {item.TeamName}");

                    foreach(var element in item.Users)
                    {
                        Console.WriteLine($" {element.FirstName} {element.LastName}   [Registered : {element.RegisteredAt}]");
                    }

                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetNumberOfTasksOnUsersProject()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var response = await client.GetAsync($"api/projects/tasks/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                var list = await response.Content.ReadAsAsync<List<UserProjectsTasksDto>>();

                Console.WriteLine("User's projects: ");

                foreach (var item in list)
                {
                    Console.WriteLine($"Project Id : {item.ProjectId}  Project name : {item.ProjectName}\n Amount of tasks : {item.TasksCount}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetUsersWithTasksSorted()
        {
            try
            {
                var response = await client.GetAsync($"api/users/tasksSorted");

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Error. {response.StatusCode}");
                    return;
                }

                var list = await response.Content.ReadAsAsync<List<UserWithTasksSortedDto>>();

                foreach(var item in list)
                {
                    Console.WriteLine($"User Id : {item.User.Id}  User name : {item.User.FirstName}");

                    foreach(var element in item.Tasks)
                    {
                        Console.WriteLine($"Task Id : {element.Id}  Task name : {element.Name}");
                    }

                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async System.Threading.Tasks.Task GetUserDataStrusture()
        {
            Console.Write("Enter user Id  : ");
            var id = Convert.ToInt32(Console.ReadLine());

            var response = await client.GetAsync($"api/users/userDataStruct/{id}");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            UserDataStructureDto result = await response.Content.ReadAsAsync<UserDataStructureDto>();

            Console.WriteLine($"User Found: {result.User.FirstName} {result.User.LastName} ");
            Console.WriteLine($"User's last project: #{result.LastProject.Id} : {result.LastProject.Name} | Created at {result.LastProject.CreatedAt}");
            Console.WriteLine($"Amount of user's tasks: {result.TasksCount}");
            Console.WriteLine($"Amount of user's not finished tasks: {result.NotFinishedTasksCount}");
            Console.WriteLine($"Longest user's task : #{result.LongestTask.Id} : \"{result.LongestTask.Name}\" | " +
                $"Created at {result.LongestTask.CreatedAt}, finished at {result.LongestTask.FinishedAt}");
        }

        private static async System.Threading.Tasks.Task GetProjectsDataStructure()
        {
            var response = await client.GetAsync($"api/projects/projectsDataStructure");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            var result = await response.Content.ReadAsAsync<List<ProjectDataStructureDto>>();

            foreach(var item in result)
            {
                Console.WriteLine();
                Console.WriteLine($"Project Id: {item.Project.Id} : \"{item.Project.Name}\" ");
                Console.WriteLine($"Task with longest description : #{item.TaskWithLongestDescription?.Id}, Description : {item.TaskWithLongestDescription?.Description}");
                Console.WriteLine($"Task with shortest name : #{item.taskWithShortestName?.Id} \"{item.taskWithShortestName?.Name}\"");

                Console.WriteLine($"Amount of users in the project's team = {item.TeamMembersCount}");
            }
        }  
    }
}
